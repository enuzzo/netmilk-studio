# humanstxt.org/
# The humans responsible & technology colophon

# TEAM

    <Luca Colombo> -- <Frontend/Web Design/Dev>
    <Raffaele Tenneriello> -- <Frontend/Dev/Web Design>

# THANKS

    <Melissa Geli>
    <Tommaso Aiello>

# TECHNOLOGY COLOPHON

    SCSS, PUG
    Apache Server Configs, jQuery, NPM, Gulp
