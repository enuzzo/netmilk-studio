<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);


//testa se si tratta di una request AJAX
function si_tratta_di_un_fottuto_ajax() {
    return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}
// nel qual caso, procedi con la chiamata della funzione PHP che invierà la mail
if (si_tratta_di_un_fottuto_ajax()) {
	inviaMail($_POST['nome'],$_POST['telefono'],$_POST['email'],$_POST['cosavuole'],$_POST['messaggio']);
}
function inviaMail($nome, $telefono, $email, $cosavuole, $messaggio){
	// metti in pausa un secondo per dare il tempo alle animazioni su netmilk.js di caricarsi
	sleep(2);

	$destinatario = "info@netmilk.ch";
	$oggetto_mail = "Richiesta di contatto dal sito";
	if ($email == "") {
		$email = "Ha preferito non indicarlo";
		$mail_mittente = "info@netmilk.ch";
	} else {
		$mail_mittente = $email;
	}
	if ($telefono == "") {$telefono = "Ha preferito non indicarlo"; }

	$testo_mail = "<p><strong>Utente</strong>: ".$nome."</p>";
	$testo_mail.= "<p><strong>Telefono</strong>: ".$telefono."</p>";
	$testo_mail.= "<p><strong>Indirizzo email</strong>: ".$email."</p>";
	if ($cosavuole<>"") {
		$testo_mail.= "<hr><strong>Gli interessa</strong>: <ul><li> ".implode("</li><li>", $cosavuole)."</li></ul>";
	} else {
		$testo_mail.= "<hr><p>Non ha scelto nessuna delle voci del menu a tendina.</p>";
	}
	if ($messaggio<>"") {
		$testo_mail.= '<hr><p><strong>Alla domanda "Puoi anticiparci qualcosa" risponde</strong>: </p><p>'.$messaggio.'</p>';
	} else {
		$testo_mail.= '<hr><p>Alla domanda "Puoi anticiparci qualcosa" non ha ritenuto opportuno rispondere.</p>';
	}
	$header = "From: ".$nome." via Netmilk.ch <".$mail_mittente.">\n";
	$header .= "Reply-To: ".$mail_mittente."\n";
	$header .= "MIME-Version: 1.0\n";
	$header .= "Content-Type: text/html; charset=\"utf-8\"\n";
	$header .= "Content-Transfer-Encoding: 7bit\n\n";

	if(@mail($destinatario, $oggetto_mail, $testo_mail, $header)) {
		$post_data = array(
			'msg' => "Messaggio inviato correttamente",
			'res' => true
		);
	} else {
		$post_data = array(
			'msg' => "Errori nell'invio della mail",
			'res' => false
		);
	}
	echo json_encode($post_data);
}
?>
