var currentUrl = window.location.href;
var currentPage = location.pathname.split("/").slice(-1);
var currentPath = currentUrl.replace(currentPage, '');
// console.log(currentPath);
var BaseUrl = currentPath;


//variabili temporali
tempo_galleggiamento = 2000;

//-Konami
var easter_egg = new Konami('konami.html');

//-Clicca o Scorri
$(window).load(function () {
    appareClicca();
});

$('.selectpicker').selectpicker({
    noneSelectedText: 'Cosa gradisci?',
    selectAllText: 'Voglio tutto',
    deselectAllText: 'Niente di tutto ciò',
    iconBase: 'fa',
    tickIcon: 'fa-check'
});

function appareClicca() {
    setTimeout(function () {
        $(".cliccaoscorri").addClass('opacity-1 animated zoomInRight');
    }, 10000);
}

//-Icona Hamburger
$(document).on('click', '.hamburger', function () {
    $('body').toggleClass('no-scroll');
    $("#menuModal").toggleClass('bounceInDown').toggleClass('bounceOutUp');
    if ($("#menuModal").hasClass('bounceInDown')) {
        $("#menuModal").fadeIn();
    } else {
        $("#menuModal").delay(800).fadeOut(500);
    }
    $('header').toggleClass('nav-open-pre').toggleClass('closed');
});

//-Animazione in uscita dalle pagine
$(document).on('click', 'a:not(.chocolat-image, .external, .mammeta, .meshim_widget_components_mobileChatButton_TappingScreen)', function () {
    // console.log('ho cliccato bene');
    url = $(this).attr("href");
    inserisci_bowl();
    setTimeout(function () {
        $(".bowl").addClass('coperto');
        //-$('[data-aos]').css('opacity','0');
    }, 100);
    setTimeout(function () {
        window.location.href = url;
    }, tempo_galleggiamento - 100); //-la variabile tempo_galleggiamento è settata in alto in questo file
    return false;
});
// PREVENT IL CARICAMENTO DELLA CACHE DEL BACKBUTTON
$(window).bind("pageshow", function (event) {
    if (event.originalEvent.persisted) {
        window.location.reload();
    }
});

//-Video BG
$('.vidbg-box').vidbg({
    //-aggiungere formati di fallback! webm, ogg
    'mp4': 'https://netmilk.ch/img/filmati/themilk.mp4',
    'webm': 'https://netmilk.ch/img/filmati/themilk.webm',
    'poster': 'https://netmilk.ch/img/milk_splash.jpg',
}, {
    volume: 0,
    playbackRate: 1,
    autoplay: true,
    muted: true,
    loop: true,
    position: '50% 50%',
    resizing: true,
    overlay: true,
    overlayColor: '#fff',
    overlayAlpha: '0.6',
});

$('.vidbg-box-2').vidbg({
    //-aggiungere formati di fallback! webm, ogg
    'mp4': 'https://netmilk.ch/img/filmati/viktoriya_bw_mini.mp4',
    'poster': 'https://netmilk.ch/img/viktoriya.jpg',
}, {
    volume: 0,
    playbackRate: 1,
    autoplay: true,
    muted: true,
    loop: true,
    position: '50% 50%',
    resizing: true,
    overlay: true,
    overlayColor: '#002c45',
    overlayAlpha: '0.5',
});

$('.vidbg-box-3').vidbg({
    //-aggiungere formati di fallback! webm, ogg
    'mp4': 'https://netmilk.ch/img/filmati/milk_vid_bg_centered_mini.mp4',
    'poster': 'https://netmilk.ch/img/milk_splash.jpg',
}, {
    volume: 1,
    playbackRate: 1,
    autoplay: true,
    muted: true,
    loop: true,
    position: '50% 50%',
    resizing: true,
    overlay: true,
    overlayColor: '#fff',
    overlayAlpha: '0.4',
});

//-Chiusura cusotm di Chocolat
function closechocolat_openheader() {
    $("header").fadeIn(200).addClass('header-bg-active');
}

$(document).on('click', '.chocolat-overlay, .chocolat-close', function () {
    closechocolat_openheader();
});

$(document).keyup(function (e) {
    if (e.which == 27) {
        closechocolat_openheader();
    }
});





//-Animazione pulsante Close
$(document).on('mouseenter', '.nav-open-pre .hamburger', function () {
    $(this).removeClass('girabile');
});
$(document).on('mouseleave', '.nav-open-pre .hamburger', function () {
    $(this).addClass('girabile');
});

$(document).ready(function () {
    //-$('[data-aos]').css('opacity','0');
    $('body').addClass('no-scroll');


    //-Hover disattivato alle Case Studies
    if (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/Windows Phone/i)) {
        $('.case-hover').addClass('no-touch');
    }

    //-Chocolat Init
    $('.chocolat-parent').Chocolat({
        fullScreen: false,
        afterMarkup: function () {
            // $(".chocolat-bottom .chocolat-fullscreen").hide();
            $("header").removeClass('header-bg-active').fadeOut(500);
        }
    });

    //-Smooth Scroller
    $(".scroller").click(function () {
        target = $(this).attr('href');
        target_position = $(target).offset().top;
        $("html, body").animate({scrollTop: target_position}, 1800, 'easeOutQuart');
        return false;
    });

    //SLICK
    $(".slider").slick({
        autoplay: true,
        pauseOnFocus: false,
        pauseOnHover: false,
        pauseOnDotsHover: false,
        easing: 'linear',
        cssEase: 'linear',
        dots: false,
        infinite: true,
        autoplaySpeed: 0,
        speed: 5500,
        slidesToShow: 5,
        slidesToScroll: 5,
        centerMode: true,
        variableWidth: true
    });

}); // END DOCUMENT READY

//-Typed.js
$(function () {
    $(".title-caption").typed({
        strings: [
            "^150 Anche le mucche nere<br>fanno il latte bianco.",
            "^150 A colori.",
            "^150 Basato sull'omonimo romanzo.",
            "^150 Who you gonna call?",
            "^150 Disponibile senza ricetta medica.",
            "^150 Religione ufficiale in 17 stati.",
            "^150 Ogni vitellino è bello a mamma soja.",
            "^150 Quel che non sa fare il cugino dell'amico dello zio.",
            "^150 ↑ ↑ ↓ ↓ ← → ← → B A",
            "^150 Senza grassi aggiunti.",
            "^150 Provaci. Insieme a un biscotto.",
            "^150 Vacca 2 O",
            "^150 Ridiamo sul latte versato.",
            "^150 Fletto i muscoli, e sono nel vuoto.",
            "^150 AS SEEN ON TV.",
            "^150 Religione ufficiale in 17 stati.",
            "^150 Tratto da una storia vera.",
            "^150 Come Previsti da Nostradamus.",
            "^150 80% Grafica, 20% Latte.",
            "^150 Abbiamo visto Elvis.",
            "^150 La verità è qua sotto.",
            "^150 Dove nessuna mucca è mai giunta prima.",
            "^150 01101011 01110101 01100100 01101111!",
            "^150 100% Pixel riciclati.",
            "^150 ESCONO DALLE PARETI!!!",
            "^150 42",
            "^150 O CAPITANO, MIO CAPITANO"
        ],
        showCursor: true,
        cursorChar: "|",
        typeSpeed: 50,
        backSpeed: 10,
        startDelay: 1000,
        backDelay: 1300,
        shuffle: true,
        loop: true
    });
});

//-Animate on Scroll
AOS.init({
    easing: 'ease-in-out-sine',
    duration: 600
});

//-Animations on Hover
$("#rubber, .icon-logo").hover(function (e) {
    $(this).addClass('animated rubberBand');
});
$("#rubber, .icon-logo").bind("animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd", function () {
    $(this).removeClass('animated rubberBand');
});

$(".tadaaa").hover(function (e) {
    $(this).addClass('animated tada');
});
$(".tadaaa").bind("animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd", function () {
    $(this).removeClass('animated tada');
});

$(window).on('scroll', function () {
    var $this = $(this),
            $head = $('header');
    if ($this.scrollTop() > 5) {
        $head.addClass('header-bg-active');
    } else {
        $head.removeClass('header-bg-active');
    }
});

$(window).load(function () {
    $(".bowl").removeClass('coperto');
    setTimeout(function () {
        $('body').removeClass('no-scroll');
        $(".bowl, .loading-icon").remove();
        //- $('[data-aos]').css('opacity','1');
    }, tempo_galleggiamento);
});



//////////////////////////////////////////////////////
//////////////// MODULO DI CONTATTO //////////////////
//////////////////////////////////////////////////////

// Attiva i POPOVER
$(function () {
    $('.avviso').popover();
});
// Inizializza il plugin intlTelInput per far comparire le bandiere della nazione
$("#telefono").intlTelInput({
      defaultCountry: "ch",
      nationalMode: false,
      preferredCountries: ['ch', 'it'],
    //  responsiveDropdown: true,
    dropdownContainer: "#drop-down-bandiere",
      preventInvalidNumbers: true,
      preventInvalidDialCodes: true,
    separateDialCode: true,
    // onlyCountries: ["ch", "it"],
    onlyCountries: ["al", "ad", "at", "by", "be", "ba", "bg", "hr", "cz", "dk", "ee", "fo", "fi", "fr", "de", "gi", "gr", "va", "hu", "is", "ie", "it", "lv", "li", "lt", "lu", "mk", "mt", "md", "mc", "me", "nl", "no", "pl", "pt", "ro", "ru", "sm", "rs", "sk", "si", "es", "se", "ch", "ua", "gb"],
    initialCountry: "auto",
    geoIpLookup: function (callback) {
        $.get("https://ipinfo.io", function () {}, "jsonp").always(function (resp) {
            var countryCode = (resp && resp.country) ? resp.country : "";
            callback(countryCode);
        });
    },
    utilsScript: BaseUrl + "bower_components/intl-tel-input/build/js/utils.js"
});
//CONSENTI SOLO NUMERI, PUNTI, E SEGNO + NEI CAMPI FORM CHE RICHIAMANO QUESTA FUNZIONE
function convalidastocazzo(evt) {
    // console.log(evt.key)
    if (evt.key === "ArrowUp" || evt.key === "ArrowRight" || evt.key === "ArrowDown" || evt.key === "ArrowLeft" || evt.key === "Backspace" || evt.key === "Delete" || evt.key === "Escape" || evt.key === "Tab") {
        // non impedire niente
    } else {
        var theEvent = evt || window.event;
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
        var regex = /^[0-9.+]+$/;
        // var regex = /[0-9]|\./;
        if (!regex.test(key)) {
            // alert("ATTENZIONE!\n\nQuesto campo accetta solo valori numerici e punti, quindi scordati di usare la virgola per i decimali, DEVI USARE IL PUNTO.\n\n(tranquillo, tanto al cliente verranno mostrate le virgole).");
            theEvent.returnValue = false;
            if (theEvent.preventDefault)
                theEvent.preventDefault();
        }
    }
}
// CONTROLLA e POI INVIA IL FORM
$(document).on("click", "#contatti", function () {
    $('.errore').removeClass("errore");
    $('.avviso').popover("hide");
    $('.popover').remove();

    if ($('#nome').val() === "") {
        $('#nome').addClass("errore");
        $('.avviso-nome').addClass("avviso").popover("show");
        return false;
    }
    if ($('#telefono').val() === "" && $('#email').val() === "") {
        $('#telefono').addClass("errore");
        $('#email').addClass("errore");
        $('.avviso-telefono').addClass("avviso").popover("show");
        return false;
    }
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if (!emailReg.test($("#email").val())) {
        $('#email').addClass("errore");
        $('.avviso-email').addClass("avviso").popover("show");
        return false;
    }
    $('body').addClass('no-scroll');
    $('<div class="semitransparent"></div>').insertAfter(".comincia");
    inserisci_bowl();
    $('.semitransparent').fadeTo("slow", 0.9);


    // Possiamo procedere con l'invio del modulo tramite ajax, perché non c'è stato alcun errore, altrimenti il return false avrebbe bloccato lo script
    var data = {
        nome: $("#nome").val(),
        telefono: $("#telefono").val(),
        email: $("#email").val(),
        cosavuole: $("#topic").val(),
        messaggio: $("#messaggio").val()
    };

    $.ajax({
        type: "POST",
        dataType: "json",
        url: BaseUrl + "cominciamo.php",
        data: data,
        success: function (response) {
            $("#nome_mittente").html($("#nome").val());
            $('.nascondimi').addClass('animated zoomOutLeft');
            $('.bowl, .loading-icon').fadeTo("300", 0);
            setTimeout(function () {
                $('.semitransparent').fadeTo("600", 0);
                $('body').removeClass('no-scroll');
                $(".bowl, .loading-icon, .semitransparent").remove();
            }, 1000);
            setTimeout(function () {
                $('.mostrami').addClass('opacity-1 animated bounceInRight');
            }, 500);
        },
        error: function (errore) {
            console.log(errore);
        }
    });

});

//////////////////////////////////////////////////////
////////////// FINE MODULO DI CONTATTO ///////////////
//////////////////////////////////////////////////////



//-function per inserire dinamnicamente (così da poterlo poi rimuovere senza che disturbi lo z-index) l'elemento bowl in pagina
function inserisci_bowl() {
    codice_bowl = '<div class="bowl"><div class="inner"><div class="fill"><svg version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink" x="0px" y="0px" width="200%" height="100%" viewBox="0 0 300 300" enable-background="new 0 0 300 300" xml:space="preserve"><path d="M300,300V2.5c0,0-0.6-0.1-1.1-0.1c0,0-25.5-2.3-40.5-2.4c-15,0-40.6,2.4-40.6,2.4          c-12.3,1.1-30.3,1.8-31.9,1.9c-2-0.1-19.7-0.8-32-1.9c0,0-25.8-2.3-40.8-2.4c-15,0-40.8,2.4-40.8,2.4c-12.3,1.1-30.4,1.8-32,1.9          c-2-0.1-20-0.8-32.2-1.9c0,0-3.1-0.3-8.1-0.7V300H300z" class="waveShape"></path></svg></div></div></div>';
    $(codice_bowl).insertAfter("header");
    codice_loader = '<div class="loading-icon appaiodalnulla"><div class="tazza"> <svg class="go" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 326 223.9" style="enable-background:new 0 0 326 223.9;" xml:space="preserve"> <style type="text/css"> .st0{display:none;}.st1{display:inline;}.st2{clip-path:url(#SVGID_2_);fill:#0077b9;}.st3{clip-path:url(#SVGID_2_);fill:#0091e2;}.st4{clip-path:url(#SVGID_2_);fill:none;stroke:#0091e2;stroke-width:2.145;}.st5{clip-path:url(#SVGID_2_);fill:#FFFCEA;}.st6{fill:#BEC3A7;}.st7{fill:#0077b9;}.st8{fill:#0091e2;}.st9{fill:#FFFCEA;}</style> <g id="Livello_1" class="st0"> <g class="st1"> <defs> <rect id="SVGID_1_" width="326" height="223.9"></rect> </defs> <clippath id="SVGID_2_"> <use xlink:href="#SVGID_1_" style="overflow:visible;"></use> </clippath> <path d="M122.4,181.4c-88.9,0-118.2-58.7-118.2-91.8c0-25.6,17.9-45,39.6-45h36.4l4.9,33.1c0,0-11.3-4.7-27.8-4.7 C40.7,73,32,80.5,32,95.7c0,19.7,1.7,53.6,69.8,53.6L122.4,181.4z" class="st2"></path> <path d="M33.5,113c6.9,24.8,28.7,36.3,68.3,36.3l8.5,13.2C110.3,162.6,38.4,165.3,33.5,113" class="st3"></path> <path d="M69.2,58c0,0-6,77.3,51.9,136.5c0,0,20.6,26.4,73.8,26.4c53.2,0,76-28,76-28C328.7,133.7,320.5,58,320.5,58 c0-29.8-56.3-53.9-125.7-53.9C125.5,4.1,69.2,28.2,69.2,58" class="st2"></path> <path d="M320.5,58c0-29.8-56.3-53.9-125.7-53.9C125.5,4.1,69.2,28.2,69.2,58c0,14.8,13.9,28.2,36.3,37.9 c0,0-0.1,0-0.1,0c2.5,12.3,12.8,53.8,29.7,85.5c17.5,32.8,86.6,42.6,134.7,9.1C297.1,171.5,323.1,111.9,320.5,58 M88.7,57.4 c0-26.1,47.6-47.2,106.3-47.2c58.7,0,106.3,21.1,106.3,47.2c0,26-47.6,47.2-106.3,47.2C136.3,104.6,88.7,83.5,88.7,57.4" class="st3"></path> <path d="M320.5,58c0-29.8-56.3-53.9-125.7-53.9C125.5,4.1,69.2,28.2,69.2,58c0,14.8,13.9,28.2,36.3,37.9 c0,0-0.1,0-0.1,0c2.5,12.3,12.8,53.8,29.7,85.5c17.5,32.8,86.6,42.6,134.7,9.1C297.1,171.5,323.1,111.9,320.5,58z M88.7,57.4 c0-26.1,47.6-47.2,106.3-47.2c58.7,0,106.3,21.1,106.3,47.2c0,26-47.6,47.2-106.3,47.2C136.3,104.6,88.7,83.5,88.7,57.4z" class="st4"></path> <path d="M293,69.8c0,17.8-43.6,34.7-97.9,34.7c-54.4,0-98.9-16.9-98.9-34.7c0-17.8,44.1-32.2,98.4-32.2 C248.9,37.6,293,52,293,69.8" class="st5"></path> <path d="M6.2,75.5c5.2-18.2,20.2-30.8,37.6-30.8h36.4l-4.5,15.4C75.6,60.1,12.3,54.3,6.2,75.5" class="st3"></path> </g> <g class="st1"> <path d="M134.1,60c0,0.3,0.1,0.7,0.4,1c0.9,1,3,1.4,4.7,0.9c16.1-5.5,34.4-8.4,52.9-8.4c20.4,0,40.4,3.5,57.8,10.2 c1.6,0.6,3.8,0.3,4.8-0.7c1-1,0.5-2.4-1.2-3c-18.5-7.1-39.7-10.8-61.4-10.8c-19.6,0-39,3.1-56.2,8.9 C134.7,58.5,134.1,59.2,134.1,60" class="st6"></path> <path d="M152.5,73.3c0,0.3,0.2,0.7,0.4,1c0.9,1.1,3,1.4,4.7,0.9c11.3-3.8,24.1-5.9,37-5.9c12.9,0,25.7,2,37,5.9 c1.7,0.6,3.7,0.2,4.7-0.9c0.9-1.1,0.3-2.4-1.4-2.9c-12.3-4.2-26.2-6.4-40.3-6.4c-14,0-28,2.2-40.3,6.4 C153.2,71.8,152.5,72.5,152.5,73.3" class="st6"></path> <path d="M166.8,85.4c0,0.3,0.1,0.6,0.3,0.9c0.8,1.1,2.9,1.5,4.6,1c7-2.1,14.6-3.2,22.4-3.2c8.2,0,16,1.1,23.2,3.4 c1.7,0.5,3.7,0.1,4.6-1c0.8-1.1,0.1-2.4-1.6-2.9c-8.2-2.6-17-3.9-26.3-3.9c-8.8,0-17.4,1.2-25.4,3.6 C167.6,83.8,166.8,84.6,166.8,85.4" class="st6"></path> </g> </g> <g id="Livello_2"> <g> <path d="M122,181.7C33,181.7,3.8,123,3.8,89.9c0-25.6,17.9-45,39.6-45h36.4L84.6,78c0,0-11.3-4.7-27.8-4.7 c-16.5,0-25.2,7.4-25.2,22.7c0,19.7,1.7,53.6,69.8,53.6L122,181.7z" class="st7"></path> <path d="M33,113.3c6.9,24.8,28.7,36.3,68.3,36.3l8.5,13.2C109.8,162.8,37.9,165.5,33,113.3" class="st8"></path> <path d="M68.5,58.2c0,0-6,77.3,51.9,136.5c0,0,20.6,26.4,73.8,26.4c53.2,0,76-28,76-28C328,134,319.8,58.2,319.8,58.2 c0-29.8-56.3-53.9-125.7-53.9C124.8,4.3,68.5,28.5,68.5,58.2" class="st7"></path> <path d="M300.2,81.2c0,25.2-46.9,49.2-105.4,49.2c-30.3,0-57.8-6.4-77.3-16.2c-8.6-4.3-8.4-20.9-8.4-20.9 s-20.7-5.7-20.7-12.1c0-25.2,47.4-45.6,105.9-45.6C252.8,35.6,300.2,56,300.2,81.2" class="st9"></path> <path d="M5.7,75.7C10.9,57.5,26,44.9,43.3,44.9h36.4l-4.5,15.4C75.2,60.3,11.9,54.5,5.7,75.7" class="st8"></path> <g> <path d="M133.8,61.9c0,0.4,0.1,0.9,0.4,1.3c0.9,1.3,3,1.8,4.7,1.1c16.1-7,34.4-10.7,52.9-10.7 c20.4,0,40.4,4.5,57.8,13c1.6,0.8,3.8,0.4,4.8-0.9c1-1.3,0.5-3-1.2-3.8c-18.5-9-39.7-13.8-61.4-13.8c-19.6,0-39,3.9-56.2,11.4 C134.4,60,133.8,60.9,133.8,61.9" class="st6"></path> <path d="M151.2,76.8c0,0.4,0.2,0.9,0.4,1.3c0.9,1.3,3,1.8,4.7,1.1c11.3-4.9,24.1-7.5,37-7.5c12.9,0,25.7,2.6,37,7.5 c1.7,0.7,3.7,0.2,4.7-1.1c0.9-1.3,0.3-3-1.4-3.7c-12.3-5.3-26.2-8.2-40.3-8.2c-14,0-28,2.8-40.3,8.2 C151.8,74.9,151.2,75.8,151.2,76.8" class="st6"></path> <path d="M166.5,90.2c0,0.4,0.1,0.8,0.3,1.2c0.8,1.4,2.9,2,4.6,1.3c7-2.7,14.6-4,22.4-4c8.2,0,16,1.4,23.2,4.3 c1.7,0.7,3.7,0.1,4.6-1.2c0.8-1.4,0.1-3-1.6-3.7c-8.2-3.3-17-4.9-26.3-4.9c-8.8,0-17.4,1.5-25.4,4.6 C167.2,88.2,166.5,89.1,166.5,90.2" class="st6"></path> </g> <path d="M320.1,58.2c0-29.8-56.3-53.9-125.7-53.9C125,4.3,68.8,28.5,68.8,58.2c0,14.8,13.9,28.2,36.3,37.9 c0,0-0.1,0-0.1,0c2.5,12.3,12.8,53.8,29.7,85.5c17.5,32.8,86.6,42.6,134.7,9.1C296.7,171.7,322.6,112.1,320.1,58.2 M88.3,57.7 c0-26.1,47.6-47.2,106.3-47.2c58.7,0,106.3,21.1,106.3,47.2c0,26-47.6,47.2-106.3,47.2C135.9,104.8,88.3,83.7,88.3,57.7" class="st8"></path> </g> </g> </svg> <div class="ombra go"> </div></div></div>';
    setTimeout(function () {
        $(codice_loader).insertAfter("header");
    }, 5);
    setTimeout(function () {
        $(".loading-icon").removeClass('appaiodalnulla');
    }, 800);
}


//-DETECTO LA POSIZIONE DEL LATTE, E QUANDO la sua parte superiore si trova più o meno a 20px, NASCONDO IL LOADER
var moving = false;
$el = $('.bowl .inner');
$el.on('transitionend', function () {
    $(".loading-icon").remove();
    moving = true;
});

function getPosition() {
    //calcola un quarto dell'altezza dello schermo
    if (!moving) {
        if ($(".bowl .inner").length) {
            if ($(".bowl .inner").offset().top >= 20) {
                $(".loading-icon.stoentrando").addClass('devouscire');
            }
        }
        window.requestAnimationFrame(getPosition);
    }
}

window.requestAnimationFrame(getPosition);
//-FINE DETECT PER NASCONDERE IL LOADER



// Quando il plugin del telefono carica il nuovo nodo in mobile dentro il container che gli ho forzato nella sua inizializzazioe,
$("#drop-down-bandiere").bind('DOMNodeInserted', function () {
    // unrappalo dal suo contenitore (che contribuiva a darci problemi) e spostalo subito dopo selected flags, così come avviene nella versione desktop
    $('ul.country-list').unwrap().insertAfter('.selected-flag');
    //per evitare poi che sia troppo corto o troppo lungo, dai all'elenco delle flags la stessa larghezza degli input del modulo (preso a riferimento il promo: "#nome")
    $('ul.country-list').width($('#nome').outerWidth());
});


//- Drawing animation Netmilk Logo Home
function tuttobianco(e) {
    $("#logo-w-ani").attr("class", "finished");
}
$(document).ready(function () {
    if ($("#logo-w-ani").length) {// ATTIVO VIVUS SOLO SE TROVO IN PAGINA L'ELEMENTO "logo-w-ani", ALTRIMENTI NELLE PAGINE DOVE NON ESISTE, SI GENERAVA UN ERRORE JAVASCRIPT
        new Vivus('logo-w-ani', {
            duration: 400,
            type: 'delayed',
            start: 'autostart'},
                tuttobianco
                );
    }
});

$(window).load(function () {
    $(".meshim_widget_components_mobileChatButton_TappingScreen").addClass('mammeta');
});

$(document).on("click", ".meshim_widget_components_mobileChatButton_TappingScreen", function (stocazzo) {
    stocazzo.preventDefault;
    // alert('ciao');
    return false;
});

$(window).load(function(){
    $(".vidbg-container video").css("visibility", "visible");
    $(".vidbg-container video").css("opacity", 1);
});
