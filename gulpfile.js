var gulp = require('gulp'),
	sass = require('gulp-ruby-sass'),
	jade = require('gulp-jade'),
	pug = require('gulp-pug'),
	notify = require('gulp-notify'),
	cleanCSS = require('gulp-clean-css'),
	htmlmin = require('gulp-htmlmin');
	concat = require('gulp-concat'),
	addsrc = require('gulp-add-src'),
	order = require('gulp-order'),
	autoprefixer = require ('gulp-autoprefixer'),
	uglify = require('gulp-uglify'),
	browserSync = require('browser-sync'),
	rename = require('gulp-rename'),
	sourcemaps = require('gulp-sourcemaps'),
  plumber = require('gulp-plumber'),
	imagemin = require('gulp-imagemin');
	imageminGuetzli = require('imagemin-guetzli');
	reload = browserSync.reload;

// Image min
gulp.task('minimage', () =>
    gulp.src('img/**/*')
        .pipe(imagemin([imageminGuetzli()]))
        .pipe(gulp.dest('dist/img'))
);

//-Sass Task
gulp.task('sass', function() {
    return sass('dev/scss/netmilk.scss', {
    style: 'compressed',
    loadPath: 'bower_components',
    sourcemap: true
    })
    .pipe(notify("Task: sass"))
    .pipe(autoprefixer())
		.pipe(cleanCSS())
		.pipe(rename({suffix:".min"}))
    .pipe(sourcemaps.write('maps', {
          includeContent: false,
          sourceRoot: '/dev/scss/'
      }))
			.on('error', sass.logError)
		.pipe(gulp.dest('css'))
		.pipe(browserSync.stream());
});

//-Jade Task
gulp.task('jade', function() {
	return gulp.src('dev/jade/**/*.jade')
		.pipe(notify("Task: jade"))
    .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
		.pipe(jade({pretty: false}))
		.pipe(htmlmin({collapseWhitespace: true}))
		.pipe(gulp.dest('./'))
		.pipe(browserSync.stream());
});

//-Pug Task
gulp.task('pug', function buildHTML() {
  return gulp.src('dev/pug/**/*.pug')
  .pipe(pug({  }))
	.pipe(notify("Task: pug"))
	.pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
	.pipe(htmlmin({collapseWhitespace: true}))
	.pipe(gulp.dest('./'))
	.pipe(browserSync.stream());
});

//-JS Task
gulp.task('js', function(){
  return gulp.src([
    'dev/js/*.js',
		'bower_components/jquery/dist/jquery.min.js',
		'bower_components/jquery-ui/jquery-ui.min.js',
		'bower_components/aos/dist/aos.js',
		'bower_components/slick-carousel/slick/slick.min.js',
		'bower_components/lazysizes/lazysizes.min.js',
		'bower_components/chocolat/dist/js/jquery.chocolat.min.js',
		'bower_components/typed.js/dist/typed.min.js',
		'bower_components/bootstrap-sass/assets/javascripts/bootstrap/dropdown.js',
		'bower_components/bootstrap-select/js/bootstrap-select.js',
		'bower_components/bootstrap-sass/assets/javascripts/bootstrap/tooltip.js',
		'bower_components/bootstrap-sass/assets/javascripts/bootstrap/popover.js',
		'bower_components/bootstrap-sass/assets/javascripts/bootstrap/modal.js',
		'bower_components/intl-tel-input/build/js/intlTelInput.min.js',
		'bower_components/vivus/dist/vivus.min.js',
		// 'bower_components/vidbg/dist/vidbg.min.js',
		'bower_components/vidbg/dist/vidbg.js',
		'bower_components/konami-js/konami.js'
    ])
    .pipe(notify("Task: JS"))
    //-.pipe(jshint())
    //-.pipe(jshint.reporter('default'))
    .pipe(order([
      "jquery.min.js",
			"jquery-ui.min.js",
			"lazysizes.min.js",
			"konami.js",
			"jquery.chocolat.min.js",
			"dropdown.js",
			"bootstrap-select.js",
			"tooltip.js",
			"popover.js",
			"modal.js",
			"intlTelInput.min.js",
			"vivus.min.js",
			// "vidbg.min.js",
			"vidbg.js",
			"aos.js",
			"slick.min.js",
			"typed.js",
			"netmilk.js"
    ]))
    .pipe(uglify())
    .pipe(concat('netmilk.min.js'))
    .pipe(gulp.dest('js'))
    .pipe(browserSync.stream());
});

//-Browsersync
gulp.task('serve', function() {
    browserSync.init({
				reloadDelay: 15000,
        server: {
            baseDir: "./"
        }
    });
});

//-Watch
gulp.task('watch', function () {
   gulp.watch('dev/scss/**/*.scss', ['sass']);
  //- gulp.watch("dev/jade/**/*.jade", ['jade']);
	 gulp.watch("dev/pug/**/*.pug", ['pug']);
   gulp.watch("dev/js/*.js", ['js']);
	//  gulp.watch("*.html").on('change', browserSync.reload);
});


//-Default: Watch and Browsersync
gulp.task('default', ['serve', 'watch']);

//Gulp Compiler
gulp.task('compila', ['pug', 'sass', 'js']);
